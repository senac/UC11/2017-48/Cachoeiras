package br.com.senac.cachoeira.view;

import android.content.Intent;
import android.graphics.Bitmap;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;

import br.com.senac.cachoeira.R;
import br.com.senac.cachoeira.model.Cachoeira;

public class NovoActivity extends AppCompatActivity {

    public static final  int  REQUEST_IMAGE_CAPTURE = 1 ;

    private static Bitmap imagem   ;

    public static Bitmap getImagem(){
        return imagem;
    }


    private ImageView imageView ;
    private EditText editTextNome ;
    private EditText editTextInformacoes ;
    private RatingBar ratingBarClassificacao ;
    private Button buttonSalvar ;

    private Cachoeira cachoeira ;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_novo);
        imageView  = findViewById(R.id.foto);
        editTextNome = findViewById(R.id.nome);
        editTextInformacoes = findViewById(R.id.informacoes);
        ratingBarClassificacao = findViewById(R.id.rtClassificacao);
        buttonSalvar = findViewById(R.id.btnSalvar);

    }









    public void capturarImagem(View view){

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE) ;
        if(intent.resolveActivity(getPackageManager()) != null ){
            startActivityForResult(intent , REQUEST_IMAGE_CAPTURE);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK){
            Bundle extras = data.getExtras()  ;
            imagem =  (Bitmap) extras.get("data") ;
            imageView.setImageBitmap(imagem);

        }


    }


    public void salvar (View view){

        String nome = editTextNome.getText().toString() ;
        String informacao = editTextInformacoes.getText().toString() ;
        float classificacao = ratingBarClassificacao.getRating() ;
        Bitmap bmp = imageView.getDrawingCache() ;

        cachoeira = new Cachoeira();
        cachoeira.setNome(nome);
        cachoeira.setInformocoes(informacao);
        cachoeira.setClassificacao(classificacao);
        cachoeira.setImagem(bmp);



        // voltar main activity
        Intent intent = new Intent();
        intent.putExtra(MainActivity.CACHOEIRA , cachoeira) ;
        setResult(RESULT_OK , intent);

        finish();



    }






























}
